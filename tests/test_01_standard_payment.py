#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Standard_payment_01 import StandardPayment01
from TestData.Brana_test_data import HomePageData
import time
import pytest

class Test01(BaseClass):

    def test_standard_payment(self, getData):
        # definování prvního testu: Standardní platba s možností uložit kartu
        tc01 = StandardPayment01(self.driver)
        # logger
        log = self.getLogger()
        tc01.select_test().click()
        tc01.confirm_test().click()
        self.explicit_wait_function_ID('creditcard')
        payment_buttons = tc01.payment_methods()
        payments = ['button-pay-mpass', 'button-pay-csob', 'button-pay-era']
        for pb in payment_buttons:
            assert pb.get_attribute("class") in payments
        assert len(payment_buttons) == 3
        tc01.fill_card_number().send_keys(getData["card_number"])
        time.sleep(1)
        tc01.fill_expiration().send_keys(getData["expiration"])
        tc01.fill_cvc().send_keys(getData["cvc"])
        tc01.save_card_check().click()
        tc01.save_card_name().send_keys(getData["card_name"])
        log.info("Card number: " + getData["card_number"] + " Expiration: " + getData["expiration"]+" CVC: " +
                 getData["cvc"])
        tc01.confirm_payment_form().submit()
        self.explicit_wait_function_text('//strong[text()="4 (APPROVED)"]')
        tc01.redirect_to_start()

    @pytest.fixture(params=HomePageData.test_01_standard_payment)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param