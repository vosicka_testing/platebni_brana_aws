#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Payment_err100_22 import PaymentErr22
from TestData.Brana_test_data import HomePageData
import time
import pytest

class Test22(BaseClass):

    def test_standard_payment(self, getData):
        # Platba pomocí pt@shop (ČSOB)
        tc22 = PaymentErr22(self.driver)
        # logger
        log = self.getLogger()
        tc22.select_test().click()
        tc22.confirm_test().click()
        try:
            assert tc22.error_block().is_displayed()
            log.info("Error message 100 displayed - OK")
            state = True
        except:
            log.warning("No error message - NOK")
            state = False
        assert state
        tc22.redirect_to_start()

    @pytest.fixture(params=HomePageData.test_14_bet_api)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param