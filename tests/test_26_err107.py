#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Payment_err107_26 import PaymentErr26
from TestData.Brana_test_data import HomePageData
import time
import pytest


class Test26(BaseClass):

    def test_standard_payment(self, getData):
        # Statická stránka pro err107 (červená chyba)
        tc26 = PaymentErr26(self.driver)
        # logger
        log = self.getLogger()
        tc26.select_test().click()
        tc26.confirm_test().click()
        try:
            assert tc26.error_block().is_displayed()
            log.info("Message 107 displayed - Platba nebyla uhrazena - OK")
            state = True
        except:
            log.warning("No message - NOK")
            state = False
        assert state

        tc26.redirect_to_start()

    @pytest.fixture(params=HomePageData.test_14_bet_api)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param