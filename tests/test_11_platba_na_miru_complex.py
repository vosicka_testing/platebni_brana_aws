#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Platba_na_miru_11 import PlatbaNaMiru11
from pageObjects.Platba_na_miru_link_11 import PlatbaNaMiruLink11
from pageObjects.Platebni_tlacitko import PlatebniTlacitko
from TestData.Brana_test_data import HomePageData
import time
import pytest

class Test11(BaseClass):
    @pytest.mark.skip(reason="no way of currently testing this")
    def test_standard_payment(self, getData):
        # Platba na miru
        tc11 = PlatbaNaMiru11(self.driver)
        # Proklik na link platby na miru
        tc11_link = PlatbaNaMiruLink11(self.driver)
        # Platebni tlacitko CSOB/ERA
        pt = PlatebniTlacitko(self.driver)
        # logger
        log = self.getLogger()
        tc11.select_test().click()
        tc11.confirm_test().click()
        tc11_link.select_test().click()
        self.explicit_wait_function_ID('creditcard')
        payment_buttons = tc11.payment_methods()
        payments = ['button-pay-mpass', 'button-pay-csob', 'button-pay-era']
        try:
            for pb in payment_buttons:
                assert pb.get_attribute("class") in payments
            assert len(payment_buttons) == 3
            log.info("Dostupne vsechny platebni metody - OK")
            not_found = True
        except:
            log.warning("Nejsou dostupne vsechny platebni metody - NOK")
            not_found = False
        assert not_found

        if getData["type"] == "omnibox":
            tc11.fill_card_number().send_keys(getData["card_number"])
            time.sleep(1)
            tc11.click_show_popup_error().click()
            tc11.fill_expiration().send_keys(getData["expiration"])
            tc11.fill_cvc().send_keys(getData["cvc"])
            try:
                tc11.save_card_check().click()
                not_found = False
            except:
                log.warning("Platba bez moznosti ulozit kartu  - podle scenare OK")
                not_found = True
            assert not_found
            log.info("Card number: " + getData["card_number"] + " Expiration: " + getData["expiration"]+" CVC: " +
                     getData["cvc"])
            tc11.confirm_payment_form().submit()
            self.explicit_wait_function_text('//strong[text()="4 (APPROVED)"]')
        elif getData["type"] == "button-pay-csob" or getData["type"] == "button-pay-era":
            pt.payment_pt(getData["type"]).click()
            self.verifyLinkPresence("Confirm")
            buttons = pt.process_buttons()
            if getData["set_test"] == 0 or getData["set_test"] == 3 or getData["set_test"] == 5:
                buttons[getData["set_test"]].click()
                log.info("Akceptováno - stav CLEARED: platebni tlacitko "+getData["type"]+" - OK")
                self.explicit_wait_function_text('//strong[text()="8 (CLEARED)"]')
                tc11.redirect_to_start()

            elif getData["set_test"] == 1 or getData["set_test"] == 2 or getData["set_test"] == 4 or \
                    getData["set_test"] == 6 or getData["set_test"] == 7:
                buttons[getData["set_test"]].click()
                log.info("Platba zamitnuta: platebni tlacitko "+getData["type"]+" - OK")
                self.explicit_wait_function_ID('system-warning')
                tc11.redirect_to_start()
        elif getData["type"] == "button-pay-mpass":
            pt.payment_pt(getData["type"]).click()
            # cekani z duvodu zobrazeni modalniho okna
            time.sleep(2)
            self.switchToFrame()
            self.driver.find_element_by_xpath("//*[@value='Continue']").click()
            try:
                self.explicit_wait_function_text('//strong[text()="4 (APPROVED)"]')
                log.info("Akceptováno - stav APPROVED " + getData["type"] + " - OK")
                mpass = True
            except:
                log.warning("Platba neprovedena - " + getData["type"] + " - NOK")
                mpass = False
            assert mpass
        tc11.redirect_to_start()

    @pytest.fixture(params=HomePageData.test_11_platba_na_miru)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param
