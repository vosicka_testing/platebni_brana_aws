#!/usr/bin/python
# -*- coding: utf-8 -*-

import pytest
from selenium import webdriver
import time
from xvfbwrapper import Xvfb
from pyvirtualdisplay import Display

driver = None

def pytest_addoption(parser):
    parser.addoption(
        "--browser_name", action="store", default="chrome"
    )


@pytest.fixture(scope="class")
def setup(request):
    global driver

    value = request.config.getoption("--browser_name")
    if value == "firefox":
        driver = webdriver.Firefox(
            executable_path=r'C:\Users\vosicka\Documents\MEGAsync\Tests\PythonSelFramework\drivers\geckodriver.exe')
    elif value == "chrome":
        driver = webdriver.Chrome(
            #executable_path=r'C:\Users\vosicka\Documents\MEGAsync\Tests\PythonSelFramework\drivers\chromedriver.exe')
            executable_path='/home/ec2-user/chromedriver')
    elif value == "edge":
        driver = webdriver.Edge(
            executable_path=r'C:\Users\vosicka\Documents\MEGAsync\Tests\PythonSelFramework\drivers\msedgedriver.exe')
    elif value == "opera":
        driver = webdriver.Opera(
            executable_path=r'C:\Users\vosicka\Desktop\operadriver.exe')
    driver.implicitly_wait(5)
    driver.get("https://monet:mips@test-mplatebnibrana.monetplus.cz/testcases")
    driver.maximize_window()

    request.cls.driver = driver
    yield

    time.sleep(3)
    driver.quit()


