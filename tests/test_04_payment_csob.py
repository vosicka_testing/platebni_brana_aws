#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Payment_only_csob_04 import PaymentOnlyCsob04
from TestData.Brana_test_data import HomePageData
import time
import pytest


class Test04(BaseClass):

    def test_standard_payment(self, getData):
        # definování prvního testu: Standardní platba s možností uložit kartu
        tc04 = PaymentOnlyCsob04(self.driver)
        # logger
        log = self.getLogger()
        tc04.select_test().click()
        tc04.confirm_test().click()
        self.explicit_wait_function_ID('creditcard')
        payment_buttons = tc04.payment_methods()
        payments = ['button-pay-mpass', 'button-pay-csob', 'button-pay-era']
        for pb in payment_buttons:
            assert pb.get_attribute("class") in payments
        assert len(payment_buttons) == 3
        tc04.fill_card_number().send_keys(getData["card_number"])
        tc04.click_show_popup_error().click()
        time.sleep(1)
        if tc04.check_popup_error():
            log.warning("Neakceptovaná platba, " + " Card number: " + getData["card_number"] +
                     " Expiration: " + getData["expiration"] + " CVC: " + getData["cvc"] + " - podle scenare OK")
            tc04.redirect_to_start()

        else:
            tc04.fill_expiration().send_keys(getData["expiration"])
            tc04.fill_cvc().send_keys(getData["cvc"])
            log.info("Akceptovaná platba, " + " Card number: " + getData["card_number"] +
                     " Expiration: " + getData["expiration"] + " CVC: " + getData["cvc"])
            tc04.confirm_payment_form().submit()
            self.explicit_wait_function_text('//strong[text()="4 (APPROVED)"]')
            tc04.redirect_to_start()

    @pytest.fixture(params=HomePageData.test_04_only_csob)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param
