#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Payment_only_cz_sk_03 import PaymentOnlyCzSk03
from TestData.Brana_test_data import HomePageData
import time
import pytest


class Test03(BaseClass):

    def test_standard_payment(self, getData):
        # definování prvního testu: Standardní platba s možností uložit kartu
        tc03 = PaymentOnlyCzSk03(self.driver)
        # logger
        log = self.getLogger()
        tc03.select_test().click()
        tc03.confirm_test().click()
        self.explicit_wait_function_ID('creditcard')
        payment_buttons = tc03.payment_methods()
        payments = ['button-pay-mpass', 'button-pay-csob', 'button-pay-era']
        for pb in payment_buttons:
            assert pb.get_attribute("class") in payments
        assert len(payment_buttons) == 3
        tc03.fill_card_number().send_keys(getData["card_number"])
        tc03.click_show_popup_error().click()
        time.sleep(1)
        if tc03.check_popup_error():
            log.warning("Neakceptovaná platba, " + " Card number: " + getData["card_number"] +
                     " Expiration: " + getData["expiration"] + " CVC: " + getData["cvc"] + " - podle scenare OK")
            tc03.redirect_to_start()

        else:
            tc03.fill_expiration().send_keys(getData["expiration"])
            tc03.fill_cvc().send_keys(getData["cvc"])
            log.info("Akceptovaná platba, " + " Card number: " + getData["card_number"] +
                     " Expiration: " + getData["expiration"] + " CVC: " + getData["cvc"])
            tc03.confirm_payment_form().submit()
            self.explicit_wait_function_text('//strong[text()="4 (APPROVED)"]')
            tc03.redirect_to_start()

    @pytest.fixture(params=HomePageData.test_03_only_cz_sk)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param
