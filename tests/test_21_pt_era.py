#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.Payment_pt_csob_21 import PaymentPtCsob21
from TestData.Brana_test_data import HomePageData
import time
import pytest

class Test21(BaseClass):

    def test_standard_payment(self, getData):
        # Platba pomocí pt@shop (Poštovní spořitelna)
        tc21 = PaymentPtCsob21(self.driver)
        # logger
        log = self.getLogger()
        tc21.select_test().click()
        tc21.confirm_test().click()
        payment_buttons = tc21.payment_methods()
        payments = ['button-pay-mpass', 'button-pay-csob', 'button-pay-era']
        for pb in payment_buttons:
            assert not pb.get_attribute("class") in payments
        assert len(payment_buttons) == 0
        log.info("Platba bez platebniho tlacitka i MPASS - OK")
        try:
            tc21.save_card_check().click()
            not_found = False
        except:
            log.warning("Platba bez moznosti ulozit kartu - podle scenare OK")
            not_found = True
        assert not_found
        self.verifyLinkPresence("Confirm")
        buttons = tc21.process_buttons()
        if getData["set_test"] == 0 or getData["set_test"] == 3 or getData["set_test"] == 5:
            buttons[getData["set_test"]].click()
            self.explicit_wait_function_text('//strong[text()="8 (CLEARED)"]')
            log.info("Akceptováno - stav CLEARED - OK")

        elif getData["set_test"] == 1 or getData["set_test"] == 2 or getData["set_test"] == 4 or \
                getData["set_test"] == 6 or getData["set_test"] == 7:
            buttons[getData["set_test"]].click()
            self.explicit_wait_function_text('//strong[text()="6 (DECLINED)"]')
            log.info("Platba zamitnuta - stav DECLINED - OK")

        tc21.redirect_to_start()

    @pytest.fixture(params=HomePageData.test_17_bet_api)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param