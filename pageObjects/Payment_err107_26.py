#!/usr/bin/python
# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By


class PaymentErr26:

    def __init__(self, driver):
        self.driver = driver

    link_test = (By.PARTIAL_LINK_TEXT, "Statická stránka pro err107 (červená chyba)")
    confirm_test_btn = (By.TAG_NAME, "input")
    error_element = (By.XPATH, '//*[text()="Platba nebyla uhrazena"]')

    def select_test(self):
        return self.driver.find_element(*PaymentErr26.link_test)

    def confirm_test(self):
        return self.driver.find_element(*PaymentErr26.confirm_test_btn)

    def error_block(self):
        return self.driver.find_element(*PaymentErr26.error_element)

    def redirect_to_start(self):
        return self.driver.get("https://monet:mips@test-mplatebnibrana.monetplus.cz/testcases")