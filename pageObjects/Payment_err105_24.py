#!/usr/bin/python
# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By


class PaymentErr24:

    def __init__(self, driver):
        self.driver = driver

    link_test = (By.PARTIAL_LINK_TEXT, "Statická stránka pro err105")
    confirm_test_btn = (By.TAG_NAME, "a")
    error_element = (By.XPATH, "//*[text()='Payment not found']")

    def select_test(self):
        return self.driver.find_element(*PaymentErr24.link_test)

    def confirm_test(self):
        return self.driver.find_element(*PaymentErr24.confirm_test_btn)

    def error_block(self):
        return self.driver.find_element(*PaymentErr24.error_element)

    def redirect_to_start(self):
        return self.driver.get("https://monet:mips@test-mplatebnibrana.monetplus.cz/testcases")