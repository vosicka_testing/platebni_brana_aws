#!/usr/bin/python
# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By


class PaymentEmbedded13:

    def __init__(self, driver):
        self.driver = driver

    payment_form = (By.TAG_NAME, "form")
    link_test = (By.PARTIAL_LINK_TEXT, "Platba kartou v embedded bráně s uložením a bez omniboxu")
    confirm_test_btn = (By.TAG_NAME, "input")
    payment_methods_btn = (By.CSS_SELECTOR, ".payment-methods a")
    card_number = (By.ID, "cardnumber")
    expiration = (By.ID, "expiry")
    cvc = (By.ID, "cvc")
    save_card_checkbox = (By.ID, "saveflag")
    card_name = (By.ID, "cardname")

    def select_test(self):
        return self.driver.find_element(*PaymentEmbedded13.link_test)

    def confirm_test(self):
        return self.driver.find_element(*PaymentEmbedded13.confirm_test_btn)

    def payment_methods(self):
        payment_btns = self.driver.find_elements(*PaymentEmbedded13.payment_methods_btn)
        return payment_btns

    def fill_card_number(self):
        return self.driver.find_element(*PaymentEmbedded13.card_number)

    def fill_expiration(self):
        return self.driver.find_element(*PaymentEmbedded13.expiration)

    def fill_cvc(self):
        return self.driver.find_element(*PaymentEmbedded13.cvc)

    def save_card_check(self):
        return self.driver.find_element(*PaymentEmbedded13.save_card_checkbox)

    def save_card_name(self):
        return self.driver.find_element(*PaymentEmbedded13.card_name)

    def confirm_payment_form(self):
        return self.driver.find_element(*PaymentEmbedded13.payment_form)

    def redirect_to_start(self):
        return self.driver.get("https://monet:mips@test-mplatebnibrana.monetplus.cz/testcases")
