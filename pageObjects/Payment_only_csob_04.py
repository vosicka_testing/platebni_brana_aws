#!/usr/bin/python
# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By


class PaymentOnlyCsob04:

    def __init__(self, driver):
        self.driver = driver

    payment_form = (By.TAG_NAME, "form")
    link_test = (By.PARTIAL_LINK_TEXT, "Platba pouze ČSOB kartou (err208 obecná)")
    confirm_test_btn = (By.TAG_NAME, "input")
    payment_methods_btn = (By.CSS_SELECTOR, ".payment-methods a")
    card_number = (By.ID, "cardnumber")
    card_number_popup_error = (By.CLASS_NAME, "card-brand-menu")
    card_number_popup = (By.CLASS_NAME, "error-popup-cardnumber")
    expiration = (By.ID, "expiry")
    cvc = (By.ID, "cvc")

    def select_test(self):
        return self.driver.find_element(*PaymentOnlyCsob04.link_test)

    def confirm_test(self):
        return self.driver.find_element(*PaymentOnlyCsob04.confirm_test_btn)

    def payment_methods(self):
        payment_btns = self.driver.find_elements(*PaymentOnlyCsob04.payment_methods_btn)
        return payment_btns

    def fill_card_number(self):
        return self.driver.find_element(*PaymentOnlyCsob04.card_number)

    def click_show_popup_error(self):
        return self.driver.find_element(*PaymentOnlyCsob04.card_number_popup_error)

    def check_popup_error(self):
        return self.driver.find_element(*PaymentOnlyCsob04.card_number_popup).is_displayed()

    def fill_expiration(self):
        return self.driver.find_element(*PaymentOnlyCsob04.expiration)

    def fill_cvc(self):
        return self.driver.find_element(*PaymentOnlyCsob04.cvc)

    def confirm_payment_form(self):
        return self.driver.find_element(*PaymentOnlyCsob04.payment_form)

    def redirect_to_start(self):
        return self.driver.get("https://monet:mips@test-mplatebnibrana.monetplus.cz/testcases")
